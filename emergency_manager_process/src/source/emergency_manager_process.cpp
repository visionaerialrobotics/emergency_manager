/*!*******************************************************************************************
 *  \file       emergency_manager_process.cpp
 *  \brief      Emergency manager implementation file.
 *  \details    
 *  \authors    Alberto Rodelgo Perales
 *              Martín Molina
 *  \copyright  Copyright (c) 2019 Universidad Politecnica de Madrid
 *              All rights reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/ 

#include "emergency_manager_process.h"

EmergencyManagerProcess::EmergencyManagerProcess()
{  

   started = false;
}

EmergencyManagerProcess::~EmergencyManagerProcess()
{}

double EmergencyManagerProcess::get_moduleRate()
{ 
 return rate;
}

void EmergencyManagerProcess::ownSetUp()
{   // Configs
    //
    ros::param::get("~robot_namespace", robot_namespace);
    if ( robot_namespace.length() == 0) robot_namespace = "drone1";
    std::cout << "robot_namespace=" <<robot_namespace<< std::endl;

    ros::param::get("~frequency", rate);
}

void EmergencyManagerProcess::ownStart(){    
    std::cout<<"STARTING EMERGENCY MANAGER"<<std::endl;
    ros::NodeHandle n;

    //Subscribers
    emergency_event_subscriber = n.subscribe("/"+robot_namespace+"/emergency_event", 1, &EmergencyManagerProcess::emergencyEventCallback, this);
    behavior_activation_finished_sub =n.subscribe("/" + robot_namespace + "/behavior_activation_finished", 1, &EmergencyManagerProcess::behaviorActivationFinishedCallback, this);

    //Services
    activate_behavior_srv = n.serviceClient<aerostack_msgs::RequestBehaviorActivation>("/" + robot_namespace +"/request_behavior_activation");
    deactivate_behavior_srv = n.serviceClient<aerostack_msgs::RequestBehaviorDeactivation>("/" + robot_namespace +"/request_behavior_deactivation");
    stop_mission = n.serviceClient<std_srvs::Empty>("/"+robot_namespace+"/python_based_mission_interpreter_process/stop");

    started = true;
}

void EmergencyManagerProcess::ownStop(){   
    started = false;
}

void EmergencyManagerProcess::ownRun()
{

}
bool EmergencyManagerProcess::isStarted()
{
    return started;
}

//Callbacks
void EmergencyManagerProcess::emergencyEventCallback(const aerostack_msgs::StringStamped& msg){
    //Get msg
    std::string belief_predicate = ""; 
    std::string belief_value = ""; 
    bool pred_bool = true;
    bool pred_value = false;
    for (auto x : msg.data){ 
        if (x == '(') pred_bool = false;
        if (x == ')') pred_value = false;
        if (pred_bool) belief_predicate = belief_predicate + x; 
        if (pred_value && x != ' ') belief_value = belief_value + x; 
        if (x == ',') pred_value = true;   
    }

    aerostack_msgs::RequestBehaviorActivation behavior_request;

    if(belief_predicate == "battery_level"){
        if (belief_value == "LOW"){
            behavior_request.request.behavior.name = "LAND";
        }
    } 
    //Stop mission
    std_srvs::Empty stop_mission_msg;
    stop_mission.call(stop_mission_msg);
    //Activate behavior
    activate_behavior_srv.call(behavior_request);

}

void EmergencyManagerProcess::behaviorActivationFinishedCallback(const aerostack_msgs::BehaviorActivationFinished &message)
{
    //message.name  //Behavior name
    switch(message.termination_cause){
        case 1: //GOAL_ACHIEVED
        break;
        case 2: //TIME_OUT
        break;
        case 3: //WRONG_PROGRESS
        break;
        case 4: //PROCESS_FAILURE
        break;
        case 5: //INTERRUPTED
        break;                
    }

}