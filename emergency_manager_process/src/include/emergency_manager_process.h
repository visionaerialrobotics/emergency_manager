/*!*******************************************************************************************
 *  \file       emergency_manager_process.h
 *  \brief      Emergency manager implementation file.
 *  \details    
 *  \authors    Alberto Rodelgo Perales
 *              Martín Molina
 *  \copyright  Copyright (c) 2019 Universidad Politecnica de Madrid
 *              All rights reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/ 


#ifndef emergency_manager_process
#define emergency_manager_process

#include <string>
#include <robot_process.h>
#include "ros/ros.h"
#include <aerostack_msgs/StringStamped.h>
#include <aerostack_msgs/RequestBehaviorActivation.h>
#include <aerostack_msgs/RequestBehaviorDeactivation.h>
#include <aerostack_msgs/BehaviorCommand.h>
#include <aerostack_msgs/BehaviorActivationFinished.h>
#include <yaml-cpp/yaml.h>

class EmergencyManagerProcess : public RobotProcess
{
public:
    EmergencyManagerProcess();
    ~EmergencyManagerProcess();

    bool isStarted();
    double get_moduleRate();  
protected:
    bool resetValues();    
private: /*RobotProcess*/
    void ownSetUp();
    void ownStart();
    void ownStop();
    void ownRun();

    double rate;
    bool started;
    std::string robot_namespace;

    ros::Subscriber emergency_event_subscriber;
    ros::Subscriber behavior_activation_finished_sub;    
    ros::ServiceClient activate_behavior_srv;
    ros::ServiceClient deactivate_behavior_srv;
    ros::ServiceClient stop_mission;

    aerostack_msgs::StringStamped emergency_event_msg;

//Callbacks
    void emergencyEventCallback(const aerostack_msgs::StringStamped& msg);
    void behaviorActivationFinishedCallback(const aerostack_msgs::BehaviorActivationFinished &message);
};

#endif 



